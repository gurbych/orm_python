import unittest
from entity import *
from mocks  import *
from models import *

class EntityTest(unittest.TestCase):
    def setUp(self):
        Entity.db   = DbMock()

    def test_save(self):
        post = Post()
        post.title   = 'title'
        post.content = 'content'
        self.cursor = post._Entity__cursor

        queries = [
            Query('insert into "post" (post_content, post_title) values (%s, %s) returning post_id', ('content', 'title')),
            Query('update "post" set post_content=%s, post_title=%s where post_id=%s', ('content', 'title', 5)),
        ]

        for i in xrange(0, 2):
            post.save()

            self.assertEqual(self.cursor.last_query, queries[i].query)
            self.assertEqual(self.cursor.last_args, queries[i].args)
            self.assertEqual(post.id, 5)

    def test_delete(self):
        post = Post()
        self.assertRaises(RuntimeError, post.delete)

        post = Post(2)
        self.cursor = post._Entity__cursor
        post.delete()
        self.assertEqual(self.cursor.last_query, 'delete from "post" where post_id=%s')
        self.assertEqual(self.cursor.last_args, (2,))

    def test_list(self):
        posts = Post.all()
        expected = [
            {'id':1, 'title':'title1', 'content':'ololo1'},
            {'id':2, 'title':'title2', 'content':'ololo2'},
            {'id':3, 'title':'title3', 'content':'freakin good'}
        ]

        self.assertEqual(len(posts), 3)

        for post, result in zip(posts, expected):
            self.assertTrue(isinstance(post, Post))
            self.assertEqual(post.id, result['id'])
            self.assertEqual(post.title, result['title'])
            self.assertEqual(post.content, result['content'])

    def test_getattr(self):
        post = Post(2)
        self.assertEqual(post.title, 'title2')
        self.assertEqual(post.content, 'ololo2')

        post = Post(3)
        self.assertEqual(post.title, 'important title')
        self.assertEqual(post.content, 'beautiful content')

    def test_get_parent(self):
        post = Post(2)
        self.assertTrue(isinstance(post.category, Category))
        self.assertEqual(post.category.id, 3)
        self.assertEqual(post.category.title, 'News')

    def test_set_parent(self):
        post = Post(2)
        post.category = Category(5)
        post.save()

        self.cursor = post._Entity__cursor
        self.assertEqual(self.cursor.last_query, 'update "post" set category_id=%s where post_id=%s')
        self.assertEqual(self.cursor.last_args, (5, 2))


    def test_get_children(self):
        cat = Category(3)
        expected = [
            {'id':1, 'title':'title1', 'content':'ololo1'},
            {'id':2, 'title':'title2', 'content':'ololo2'},
            {'id':3, 'title':'title3', 'content':'freakin good'}
        ]
        posts = cat.posts
        self.assertEqual(len(posts), 3)
        for post, result in zip(posts, expected):
            self.assertTrue(isinstance(post, Post))
            self.assertEqual(post._Entity__loaded, True)
            self.assertEqual(post.id, result['id'])
            self.assertEqual(post.title, result['title'])
            self.assertEqual(post.content, result['content'])

    def test_get_siblings(self):
        post = Post(2)
        expected = [
            {'id':1, 'name':'tag1'},
            {'id':2, 'name':'tag2'},
            {'id':3, 'name':'tag3'}
        ]
        tags = post.tags
        self.assertEqual(len(tags), 3)
        for tag, result in zip(tags, expected):
            self.assertTrue(isinstance(tag, Tag))
            self.assertEqual(tag.id, result['id'])
            self.assertEqual(tag._Entity__loaded, True)
            self.assertEqual(tag.name, result['name'])



if __name__ == '__main__':
    unittest.main()
