class InvalidQueryError(Exception):
    pass

class Query(object):
    def __init__(self, query, args):
        self.query = query
        self.args  = args

    def __eq__(self, other):
        return self.query == other.query and self.args == other.args

    def __hash__(self):
        return hash(str(self))

    def __repr__(self):
        return self.query + '; ' + str(self.args)


class DbMock(object):
    def __init__(self):
        pass

    def cursor(self, *args, **kwargs):
        return CursorMock(args, kwargs)

    def commit(self):
        pass

    def rollback(self):
        pass


responces = {
    Query('select * from "post"', tuple()):
        [{'post_id':1, 'post_title':'title1', 'post_content':'ololo1', 'category_id':3},
            {'post_id':2, 'post_title':'title2', 'post_content':'ololo2', 'category_id':3},
            {'post_id':3, 'post_title':'title3', 'post_content':'freakin good', 'category_id':3}
        ],
    Query('select * from "post" where post_id=%s', (2,)):
        [{'post_id': 2, 'post_title': 'title2', 'post_content': 'ololo2', 'category_id':3}],
    Query('select * from "post" where post_id=%s', (3,)):
        [{'post_id': 3, 'post_title': 'important title', 'post_content': 'beautiful content', 'category_id':3}],
    Query('delete from "post" where post_id=%s', (2,)):
        [],
    Query('insert into "post" (post_content, post_title) values (%s, %s) returning post_id', ('content', 'title')):
        [[5]],
    Query('update "post" set post_content=%s, post_title=%s where post_id=%s', ('content', 'title', 5)):
        [],
    Query('update "post" set category_id=%s where post_id=%s', (5, 2)):
        [],
    Query('select * from "category" where category_id=%s', (3,)):
        [{'category_id':3, 'category_title':'News'}],
    Query('select * from "post" where category_id=%s', (3,)):
        [{'post_id':1, 'post_title':'title1', 'post_content':'ololo1', 'category_id':3},
            {'post_id':2, 'post_title':'title2', 'post_content':'ololo2', 'category_id':3},
            {'post_id':3, 'post_title':'title3', 'post_content':'freakin good', 'category_id':3}
        ],
    Query('select * from "tag" natural join "post__tag" where post_id=%s', (2,)):
        [{'tag_id':1, 'tag_name':'tag1'},
            {'tag_id':2, 'tag_name':'tag2'},
            {'tag_id':3, 'tag_name':'tag3'}
        ],
}

class CursorMock(object):
    def __init__(self, *args, **kwargs):
        self.rows = []
        self.lastrowid  = None

    def execute(self, query, args=[]):
        self.last_query = query.lower()
        self.last_args  = tuple(args)

        query = Query(self.last_query, self.last_args)

        try:
            self.rows = responces[query]
            return self.rows
        except KeyError:
            raise InvalidQueryError


    def fetchall(self):
        rows = self.rows
        self.rows = []
        return rows

    def fetchone(self):
        return self.rows[0]







