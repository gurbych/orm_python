import psycopg2
import psycopg2.extras
import datetime

class DatabaseError(Exception):
    pass
class NotFoundError(Exception):
    pass

class Entity(object):
    db = None

    __delete_query    = 'DELETE FROM "{table}" WHERE {table}_id=%s'
    __insert_query    = 'INSERT INTO "{table}" ({columns}) VALUES ({placeholders}) RETURNING {table}_id'
    __list_query      = 'SELECT * FROM "{table}"'
    __parent_query    = 'SELECT * FROM "{table}" WHERE {parent}_id=%s'
    __select_query    = 'SELECT * FROM "{table}" WHERE {table}_id=%s'
    __sibling_query   = 'SELECT * FROM "{sibling}" NATURAL JOIN "{join_table}" WHERE {table}_id=%s'
    __update_children = 'UPDATE "{table}" SET {parent}_id=%s WHERE {table}_id IN ({children})'
    __update_query    = 'UPDATE "{table}" SET {columns} WHERE {table}_id=%s'

    def __init__(self, id=None):
        if self.__class__.db is None:
            message = 'No database connection. Set Entity.db first.'
            raise DatabaseError(message)

        self.__cursor   = self.__class__.db.cursor(cursor_factory=psycopg2.extras.DictCursor)
        self.__fields   = {}
        self.__id       = id
        self.__loaded   = False
        self.__modified = False
        self.__class    = self.__class__.__name__
        self.__table    = self.__class.lower()

    @property
    def id(self):
        return self.__id

    @property
    def created(self):
        self.__load()
        return self.__fields['{}_created'.format(self.__table)]

    @property
    def updated(self):
        self.__load()
        return self.__fields['{}_updated'.format(self.__table)]

    def __getattr__(self, name):
        if self.__modified:
            raise RuntimeError(self.__class + ' has unsaved changes.')

        self.__load()
        if name in self._columns:
            return self._get_column(name)
        if name in self._parents:
            return self._get_parent(name)
        if name in self._children:
            return self._get_children(name)
        if name in self._siblings:
            return self._get_siblings(name)
        raise AttributeError

    def __setattr__(self, name, value):
        if name in self._columns:
            self._set_column(name, value)
        elif name in self._parents:
            self._set_parent(name, value)
        else:
            super(Entity, self).__setattr__(name, value)

    def __execute_query(self, query, args):
        try:
            self.__cursor.execute(query, args)
            self.__class__.db.commit()
        except Exception, e:
            self.__class__.db.rollback()
            raise e

    def __insert(self):
        columns = ', '.join(self.__fields)
        placeholders = ', '.join(['%s'] * len(self.__fields))

        query = self.__class__.__insert_query.format(
            table=self.__table,
            columns=columns,
            placeholders=placeholders
        )

        self.__execute_query(query, self.__fields.values())
        self.__id = self.__cursor.fetchone()[0]

    def __load(self):
        if self.__loaded:
            return

        query = self.__class__.__select_query.format(table=self.__table)
        self.__cursor.execute(query, (self.id,))

        rows = self.__cursor.fetchall()
        if len(rows) == 0:
            raise NotFoundError()

        for row in rows:
            self.__fields = dict(row)
        self.__loaded = True

    def __update(self):
        columns = ['{}=%s'.format(column) for column in self.__fields]
        columns = ', '.join(columns)
        args    = self.__fields.values() + [self.id]
        query   = self.__class__.__update_query.format(table=self.__table,
                                                       columns=columns)
        self.__execute_query(query, args)

    def _get_children(self, name):
        import models
        child_entity_name = self._children[name]
        child_table_name  = child_entity_name.lower()
        children = []
        cls = getattr(models, child_entity_name)
        query = self.__class__.__parent_query.format(table=child_table_name,
                                                     parent=self.__table)
        self.__cursor.execute(query, (self.id,))
        rows = self.__cursor.fetchall()

        for row in rows:
            child = self.__class__.__row_to_instance(cls, row)
            children.append(child)
        return children

    def _get_column(self, name):
        return self.__fields['{}_{}'.format(self.__table, name)]

    def _set_column(self, name, value):
        self.__modified = True
        self.__fields['{}_{}'.format(self.__table, name)] = value

    def _set_parent(self, name, value):
        self.__modified = True
        if isinstance(value, Entity):
            self.__fields['{}_id'.format(name)] = value.id
        elif isinstance(value, int):
            self.__fields['{}_id'.format(name)] = value
        else:
            self.__modified = False
            raise AttributeError("Value must be instance of Entity subclass or int")


    def delete(self):
        if self.id is None:
            message = 'Cannot delete an entity which has never been saved'
            raise RuntimeError(message)
        query = self.__class__.__delete_query.format(table=self.__table)
        self.__execute_query(query, (self.id,))
        self.__id = None
        self.__loaded = False
        self.__modified = False
        self.__fields = {}

    def _get_parent(self, name):
        import models
        cls = getattr(models,name.title())
        query = self.__class__.__select_query.format(table=name)
        key = '{}_id'.format(name)
        self.__cursor.execute(query, (self.__fields[key],))

        row = self.__cursor.fetchone()

        return self.__class__.__row_to_instance(cls, row)

    def _get_siblings(self, name):
        import models
        sibling_entity_name = self._siblings[name]
        sibling_table_name  = sibling_entity_name.lower()
        join_table_name = '__'.join(sorted([sibling_table_name, self.__table]))

        query = self.__class__.__sibling_query.format(
            table=self.__table,
            sibling=sibling_table_name,
            join_table=join_table_name
        )

        cls = getattr(models, sibling_entity_name)
        self.__cursor.execute(query, (self.id,))
        rows = self.__cursor.fetchall()

        siblings = []
        for row in rows:
            instance = self.__class__.__row_to_instance(cls, row)
            siblings.append(instance)
        return siblings

    @classmethod
    def all(cls):
        cursor = cls.db.cursor(cursor_factory=psycopg2.extras.DictCursor)
        query  = cls.__list_query.format(table=cls.__name__.lower())

        cursor.execute(query)
        rows = cursor.fetchall()
        instances = []
        for row in rows:
            instance = cls.__row_to_instance(cls, row)
            instances.append(instance)

        return instances

    def save(self):
        if self.id is None:
            self.__insert()
        else:
            self.__update()

        self.__modified = False

    @staticmethod
    def __row_to_instance(instance_class, row_data):
        instance = instance_class()
        instance.__fields = dict(row_data)
        instance.__id = row_data["{}_id".format(instance.__table)]
        instance.__loaded = True

        return instance


