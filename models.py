from entity import *

class Section(Entity):
    _columns  = ['title']
    _parents  = []
    _children = {'categories': 'Category'}
    _siblings = {}

class Category(Entity):
    _columns  = ['title']
    _parents  = ['section']
    _children = {'posts': 'Post'}
    _siblings = {}

class Post(Entity):
    _columns  = ['content', 'title']
    _parents  = ['category']
    _children = {'comments': 'Comment'}
    _siblings = {'tags': 'Tag'}

class Comment(Entity):
    _columns  = ['text']
    _parents  = ['post', 'user']
    _children = {}
    _siblings = {}

class Tag(Entity):
    _columns  = ['name']
    _parents  = []
    _children = {}
    _siblings = {'posts': 'Post'}

class User(Entity):
    _columns  = ['name', 'email', 'age']
    _parents  = []
    _children = {'comments': 'Comment'}
    _siblings = {}


if __name__ == "__main__":
    Entity.db = psycopg2.connect("dbname=test user=xtreem password=dt3ftbfh4m")

# 1 News
# 2 Events
# 3 Places
# 4 Gallery
# 5 People
# 6 Movie

    # categories = [
    #     {'section': 1, 'title': 'Politics'},
    #     {'section': 1, 'title': 'Show-biz'},
    #     {'section': 1, 'title': 'Sport'},
    #     {'section': 1, 'title': 'Extraordinary'},
    #     {'section': 2, 'title': 'Boat racing'},
    #     {'section': 2, 'title': 'Concerts'},
    #     {'section': 2, 'title': 'Meetings'},
    #     {'section': 3, 'title': 'Libraries'},
    #     {'section': 3, 'title': 'Cinemas'},
    #     {'section': 3, 'title': 'Bars'},
    #     {'section': 3, 'title': 'Shops'},
    #     {'section': 3, 'title': 'Theatres'},
    #     {'section': 4, 'title': 'Nature'},
    #     {'section': 4, 'title': 'Girls'},
    #     {'section': 4, 'title': 'Cars'},
    #     {'section': 4, 'title': 'Wallpapers'},
    #     {'section': 5, 'title': 'Niggers'},
    #     {'section': 5, 'title': 'Whites'},
    #     {'section': 5, 'title': 'Chineeze'},
    #     {'section': 5, 'title': 'Japs'},
    #     {'section': 6, 'title': 'Fiction'},
    #     {'section': 6, 'title': 'Thriller'},
    #     {'section': 6, 'title': 'Drama'},
    # ]

    # for cat in categories:
    #     category = Category()
    #     category.title = cat['title']
    #     category.section = cat['section']
    #     category.save()

    for cat in Category.all():
        print cat.id, cat.title, cat.section.title

    for cat in Section(3).categories:
        print cat.id, cat.title
